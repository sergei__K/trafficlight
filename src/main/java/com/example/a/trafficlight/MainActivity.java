package com.example.a.trafficlight;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {
    ConstraintLayout constraintLayout;
    Button greenButton;
    Button yellowButton;
    Button redButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        constraintLayout = findViewById(R.id.main_layout);

        greenButton = findViewById(R.id.green_button);
        greenButton.setOnClickListener(onClickListener);

        yellowButton = findViewById(R.id.yellow_button);
        yellowButton.setOnClickListener(onClickListener);

        redButton = findViewById(R.id.red_button);
        redButton.setOnClickListener(onClickListener);
    }

    final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.green_button:
                    constraintLayout.setBackgroundResource(R.color.colorGreen);
                    break;
                case R.id.yellow_button:
                    constraintLayout.setBackgroundResource(R.color.colorOrange);
                    break;

                case R.id.red_button:
                    constraintLayout.setBackgroundResource(R.color.colorRed);
                    break;

            }

        }
    };
}
